EXEC = pruebas
CC = gcc
CFLAGS = -Werror -pedantic -std=c99 -g
SRC = strutil.c pila.h pila.c cola.c cola.h
OBJ = strutil.o pila.o cola.o

all: pruebas

c: $(OBJ)
	$(CC) $(CFLAGS) $(SRC) -o $(EXEC)

run: c
	./pruebas

reset: hash.o pruebas
	rm hash.o pruebas .hash.c.swp vgcore.*

remove_vgcore:
	rm vgcore.*

valgrind: pruebas
	valgrind --leak-check=full --track-origins=yes --show-reachable=yes ./pruebas
