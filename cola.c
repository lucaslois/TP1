#include "cola.h"
#include <stdlib.h>
#include <stdio.h>


typedef struct nodo nodo_t;

struct nodo {
  void* dato;
  nodo_t* nextnodo;
};

struct cola {
  nodo_t* primernodo;
  nodo_t* ultimonodo;
};


cola_t* cola_crear(void) {
  cola_t* cola = malloc(sizeof(cola_t));

  if(cola == NULL) {
    free(cola);
    return NULL;
  }
  cola->primernodo = NULL;
  cola->ultimonodo = NULL;
  return cola;
}

bool cola_esta_vacia(const cola_t *cola) {
  return cola->primernodo == NULL && cola->ultimonodo == NULL;
}

bool cola_encolar(cola_t *cola, void *valor) {
  nodo_t *nodo = malloc(sizeof(nodo_t));
  if(nodo == NULL) {
      free(nodo);
      return false;
  }
  nodo->dato = valor;
  nodo->nextnodo = NULL;

  if(cola_esta_vacia(cola) == true) {
      cola->primernodo = nodo;
      cola->ultimonodo = nodo;
  }
  else {
      cola->ultimonodo->nextnodo = nodo;
      cola->ultimonodo = nodo;
  }
  return true;
}

void* cola_desencolar(cola_t *cola) {
    if(cola_esta_vacia(cola)) {
        return NULL;
    }
    nodo_t *siguiente = cola->primernodo->nextnodo;
    void* dato = cola->primernodo->dato;

    if(cola->primernodo == cola->ultimonodo)
        cola->ultimonodo = NULL;
    free(cola->primernodo);
    cola->primernodo = siguiente;

    return dato;
}

void* cola_ver_primero(const cola_t* cola) {
    if(cola_esta_vacia(cola))
        return NULL;
    return cola->primernodo->dato;
}

void cola_destruir(cola_t *cola, void destruir_dato(void*)) {
    nodo_t* nodo_actual = cola->primernodo;
    while(nodo_actual) {
        nodo_t* siguiente = nodo_actual->nextnodo;
        if(destruir_dato) {
            destruir_dato(nodo_actual->dato);
        }
        free(nodo_actual);
        nodo_actual = siguiente;
    }
    free(cola);
}
